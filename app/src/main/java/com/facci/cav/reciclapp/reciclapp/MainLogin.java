package com.facci.cav.reciclapp.reciclapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainLogin extends AppCompatActivity {
    Button Registrarse ;
    Button Accede;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        Registrarse = (Button)findViewById(R.id.BtnRegistrarse);
        Accede = (Button)findViewById(R.id.BtnAcceder);

        Registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainLogin.this, MainRegistroUser.class);
                startActivity(intent);
            }
        });
        Accede.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(MainLogin.this, MainMenu.class);
               startActivity(intent);
            }
        });

    }
}