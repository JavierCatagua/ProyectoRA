package com.facci.cav.reciclapp.reciclapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenu extends AppCompatActivity {
Button mperfil;
Button minformacion;
Button mreciclaje;
Button mRegistros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        mperfil=(Button)findViewById(R.id.btnMU);
        minformacion=(Button)findViewById(R.id.btnINF);
        mRegistros=(Button)findViewById(R.id.btnRGT);
        mreciclaje=(Button)findViewById(R.id.btnRCJ);

mperfil.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainMenu.this, MainUsuario.class);
        startActivity(intent);
    }
});

    }

}
