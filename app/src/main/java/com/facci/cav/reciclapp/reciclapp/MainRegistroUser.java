package com.facci.cav.reciclapp.reciclapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainRegistroUser extends AppCompatActivity {
    Button Registrar ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_registro_user);
        Registrar = (Button)findViewById(R.id.BtnRegistrarse1);


        Registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainRegistroUser.this, MainMenu.class);
                startActivity(intent);
            }
        });
    }
}
